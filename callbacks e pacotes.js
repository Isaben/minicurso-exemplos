// Uso do require para importar um pacote JÁ INSTALADO!!!
const lodash = require("lodash");

// É possível verificar TODAS as funções do objeto
//console.log(lodash);

/*
    O uso das funções callback é de EXTREMA importância ao se programar em
    JavaScript. Ao declarar um parâmetro de uma função com a palavra reservada
    callback, o programa espera que naquele parâmetro será repassado uma função
    ADEQUADA para o tratamento dos dados repassados.
*/

function exemplo_callback(arg1, callback) {
    /*
        O que está acontecendo aqui é que o segundo argumento, callback, é
        esperado ser uma função. Como estamos repassando exatamente UM argumento
        para callback, então a função que será repassada por fora deve ser capaz
        de lidar com esse argumento.
    */
    callback(arg1);
}

function lidar_callback(arg1) {
    console.log("Função declarada anteriormente. Argumento é: ", arg1);
}

exemplo_callback("Oi!", lidar_callback);

// É possível passar arrow function como callback
exemplo_callback("Oi! Arrow", (argumento) => {
    console.log("Arrow function. Argumento é: ", argumento);
});

/*
    Diversas funções da biblioteca padrão da linguagem, assim como de
    terceiros, utilizam o recurso de callback. Abaixo um exemplo de 
    uso do forEach, para iterar em um array. A forEach assume
    exatamente um argumento, que é um callback de um argumento
    que assume o valor de cada elemento, em ordem, do array.
*/
let vetor = [1, 2, 3, 4, 5];
vetor.forEach((elemento) => console.log(`${elemento}`));

/*
    A lodash é um ótimo exemplo de pacote porque é quase um "cinto de utilidades",
    possuindo funções de manipulações de strings, objetos e arrays com comportamento
    semelhante entre os tipos. A seguir, alguns exemplos de uso de algumas destas
    funções. Para mais detalhes, visite https://lodash.com/docs/
*/

var usuarios = [
    { 'user': 'barney',  'age': 36, 'active': true },
    { 'user': 'fred',    'age': 40, 'active': false },
    { 'user': 'pebbles', 'age': 1,  'active': true }
];

let test_object = {
    id: "teste_id",
    name: "teste_name",
    vetor: [
        {
            name_vec: "name_vec1",
            t: 1
        },
        {
            name_vec: "name_vec2",
            t:2
        }
    ],
    another_object: {
        nani: "h-haiai"
    }
};

let test_array = [1, 2, 3, "a", "b", "c"];

console.log("Inverter objeto: ",                lodash.invert(test_object));
console.log("Reverse array: ",                  lodash.reverse(test_array));
console.log("Shuffle: ",                        lodash.shuffle(test_array));
console.log("Find em um array de objetos: ",    lodash.find(test_object.vetor, {t: 2}));
console.log("Filtrando um array de objetos: ",  lodash.filter(usuarios, ['active', true]));

/*
    Cria um simples servidor HTTP capaz de responder a requisições na rota "/"
    de GET e POST com uso dos pacotes express e body-parser.
    body-parser é um middleware do express responsável por tratar o corpo
    das requisições, de forma a ser tratado melhor no código.
    Nesse caso, estamos utilizando para parsear JSON e querystring.
*/

const express = require("express");
var bodyParser = require("body-parser");  

let app = express(); // instancia o servidor

// Configura o servidor para utilizar o parser em TODAS as requisições
app.use(bodyParser.urlencoded({extended: false}));  
app.use(bodyParser.json());  

// Começa a "escutar" a porta 3000
app.listen(3000);

// Função para requisições do tipo GET na rota "/"
app.get('/', (req, res) => {
    res.status(200).send("Hello World!");
})

// Função para requisições do tipo POST na rota "/"
app.post('/', (req, res) => {
    let answer = {
        resposta: "Resposta pelo POST!",
        enviado: req.body
    }
    res.status(200).send(answer);
})
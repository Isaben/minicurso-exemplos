
/*
    Uma simples declaração de função.
    Como JavaScript não é fortemente tipada, não existe
    especificação do tipo de retorno.
*/

function exemplo(arg1 = 50, arg2 = "Oi! Eu sou Goku!") {
    console.log(arg1);
    console.log(arg2);

    return arg1;
}

function soma_exemplo(arg1, arg2) {
    return arg2 + arg1;
}

// Não é necessário passar todos os argumentos para chamar a função!
exemplo();

// Vários exemplos de declaração de objetos
let teste = "tipo 1 de declaração";
var declaracao = "tipo 2 de declaração";
let exemplo_vetor = [1, 2, "olha, uma string!", exemplo, "MEU DEUS ATÉ UMA FUNÇÃO!"];

let soma = soma_exemplo;
let exemplo_json = {
    id: "oi!",
    func: soma_exemplo,
    json_interno: {
        id: "olha só! outro id!",
        vetor: [
            "temos um vetor!",
            12
        ]
    }
};

/*
    É possível declarar funções on-the-fly.
    Aqui está sendo declarado uma função que aceita dois argumentos,
    e retorna a subtração de ambos. Essa função está acessível a partir
    da variação exemplo_arrow_function
*/
let exemplo_arrow_function = (arg1, arg2) => arg1 - arg2;

console.log("\nExemplo arrow function :::: ",  exemplo_arrow_function(3, 4));

// Exemplos de formas de acesso de dados
console.log("\nAcesso como objeto :::: " ,     exemplo_json.json_interno.id); 
console.log("\nAcesso como dicionário :::: ",  exemplo_json["json_interno"]["vetor"]); 
console.log("\nFunção dentro de vetor :::: " , exemplo_vetor[3]()); 
console.log("\nVariável como uma função :::: ",soma(2, 5));

// Verificar o tipo de uma variável
console.log("\nVariável é do tipo :::: ",      typeof exemplo_json);
/*
    Console é uma classe padrão do Node que controla todo
    o output da linha de comando.
    Com isso, temos acessos à alguns métodos como:
    log: funciona para exibir dados informativos no stdout,
    não necessariamente consistindo em erros.
    error: exibir informativos de erros. Dependendo do terminal,
    como os textos vão para o stderr, é possível que a cor
    seja diferente para informar.
    Sintaticamente, o ponto e vírgula não é necessário ao final da linha,
    vai de cada programador utilizar ele ou não.
    Lembrando: não é preciso de função main!
*/
console.log("Hello World!"); 

console.log([2].slice(1))


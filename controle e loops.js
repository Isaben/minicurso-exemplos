/*
    A estrutura do IF e suas peculiaridades
    Detalhe para o uso de == e ===!
*/
let teste_booleano = true;
if(teste_booleano) {
    console.log("Vai ser impresso!");
}

if(!teste_booleano)
    console.log("Não vai ser impresso!");

else if(2 == "2")
    console.log("Será?");

else
    console.log("Hmmmm....");

if(2 === "2")
    console.log ("E esse?");

if((false || true) && (false))
    console.log("Nope!");

// Operador de condicional ternário
console.log(undefined === false ? "Sim" : "Não")

function print(arg1){
    return arg1;
}

let vetor = [1, 4, 5, print("Vai!"), "Teste! Haha", {id: "meu velho..."}];

/*
    Formas de iterar em um vetor. Na prática, as estruturas nativas
    da linguagem são mais rápidas, apesar de serem menos "bonitas" no código.
*/

for(let i = 0; i < vetor.length; i++) {
    console.log("Iteração " + i + ": " + vetor[i]);
}

// Atenção com escopos e let vs var!!!!!
let index = 0;
while (index < vetor.length) {
    var vamos_testar = "Essa vai funcionar!";
    let vamos_testar2 = "Essa não!";
    index++;
}

try {
    console.log("\nO var do loop passado funcionou!" ,vamos_testar);
    console.log(vamos_testar2);
}
catch(err) {
    console.log("Já o let não :(\n");
}

let teste_json = {
    id: "O id é:... sei lá",
    numero: 42,
    outro_objeto: {
        vetor: [
            "Haha!",
            "Omae wa mou shindeiru"
        ]
    }
}

// For Loop nas propriedades de um JSON
for(let propriedade in teste_json) {
    console.log("Nome da propriedade: ", propriedade);
    console.log("Valor da propriedade: ", teste_json[propriedade]);
}

// E por fim, o simples switch!

switch(teste_json.numero) {
    case 42:
        console.log("\nSignificado da vida!");
        break;

    default:
        console.log("\nNunca serão....");
}
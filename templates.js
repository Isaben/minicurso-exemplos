const lodash = require('lodash');

function quickreplies(text = '', qrs = []) {
    qrs = zip(['content_type', 'title', 'payload', 'image_url'], qrs);
    qrs = zipToObj(qrs);

    return {
        text,
        quick_replies: qrs
    };
}

function image(url) {
    return {
        "attachment": {
            "type": "image",
            "payload": {
                "url": url
            }
        }
    };
}

function button(text, _butts) {
    return {
        "attachment": {
            "type": "template",
            "payload": {
                "template_type": "button",
                "text": text,
                "buttons": buttons(_butts)
            }
        }
    };
}

function generic(carousels = [], image_aspect_ratio = 'horizontal') {
    return {
        "attachment": {
            "type": "template",
            "payload": {
                "template_type": "generic",
                "image_aspect_ratio": image_aspect_ratio,
                "elements": createCarousel(carousels)
            }
        }
    };
}

function qr_button(text, qrs, _buttons) {
    qrs = zip(['content_type', 'title', 'payload', 'image_url'], qrs);
    qrs = zipToObj(qrs);

    return {
        "attachment": {
            "type": "template",
            "payload": {
                "template_type": "button",
                "text": text,
                "buttons": buttons(_buttons)
            }
        },
        "quick_replies": qrs
    };
}

function qr_list(qrs = [], lists = [], _buttons = [], top_element_style = 'compact') {
    qrs = zip(['content_type', 'title', 'payload', 'image_url'], qrs);
    qrs = zipToObj(qrs);

    return {
        "attachment": {
            "type": "template",
            "payload": {
                "template_type": "list",
                "top_element_style": top_element_style,
                "elements": createCarousel(lists),
                "buttons": buttons(_buttons)
            },
            
        },
        "quick_replies": qrs            
    };
}

function qr_carousel(qrs = [], carousels = [], image_aspect_ratio = 'horizontal') {
    qrs = zip(['content_type', 'title', 'payload', 'image_url'], qrs);
    qrs = zipToObj(qrs);

    return {
        "attachment": {
            "type": "template",
            "payload": {
                "template_type": "generic",
                image_aspect_ratio,
                "elements": createCarousel(carousels)
            }
        },
        "quick_replies": qrs
    };
}


function createCarousel(carousels) {
    carousels = zip(['title', 'subtitle', 'buttons', 'image_url', ''], carousels);
    carousels = zipToObj(carousels);

    carousels = lodash.map(carousels, carousel => {
        if (carousel.buttons) {
            carousel.buttons = buttons(carousel.buttons);
        }

        return carousel;
    });

    return carousels;
}




function buttons(butts) {
    if (butts && !lodash.isEmpty(butts)) {
        const mapProperties = {
            postback: ['type', 'title', 'payload'],
            web_url: ['type', 'title', 'url', 'webview_height_ratio', 'messenger_extensions'],
            phone_number: ['type', 'title', 'payload']
        };

        butts = lodash.map(butts, (butt) => {
            butt = zip(mapProperties[butt[0]], butt);
            butt = zipToObj(butt);

            return butt[0];
        });

        return butts;

    }
    return [];
}

/**
 * 
 * @param {type} properties propriedades que devem conter no objeto, ex para quickreplies:
 * ['content_type', 'title', 'payload', 'image_url']
 * 
 * @param {type} features array passado para criar o objeto, ex:
 * ['text', 'title', 'payload', 'image_url']
 * 
 * @returns {unresolved}
 */
function zip(properties, features) {
    //Verifica se as propriedades esta em uma tupla de tuplas:
    //[ [ ] ]
    //caso o conjunto tiver mais de uma tupla ( [  [], [] ] ), usa a primeira index 0,
    //se nao for um conjunto de tuplas (features = [] ),
    //a transforma em: [ features ] => [  [ ] ]
    if (!lodash.isArray(features[0])) {
        features = [features];
    }

    return lodash.map(features, (feature) => {
        //junta as prorpriedades das qrs, por exemplo, com cada posiçao do array de caracteristicas passados
        //ex: qrs = [  ['tipo', 'titulo']  ]
        // zip : [  [ ['content_type', 'title', 'payload', 'image_url'],  ['text', 'titulo']  ]
        // unzip: [  ['content_type', 'text'], ['title', 'titulo'], ['payload', undefined], ['image_url', undefined] ]
        return lodash.zip(properties, feature);
    });
}

/**
 * Cria um objeto a partir de um par de array,
 * onde array[0] = nome da propriedade, array[1]= valor da propriedade
 * 
 * ex: ['content_type', 'text']
 * 
 * zipToObj => {content_type: 'text'}
 * 
 * @param {type} features propriedades que deve conter no objeto, ex para qrs
 * @returns {unresolved}
 */
function zipToObj(features) {
    let json = {};
    //Verifica se as propriedades esta em uma tupla de tuplas:
    //[ [ ] ]
    //caso o conjunto tiver mais de uma tupla ( [  [], [] ] ), usa a primeira index 0,
    //se nao for um conjunto de tuplas (features = [] ),
    //a transforma em: [ features ] => [  [ ] ]
    if (!lodash.isArray(features[0])) {
        features = [features];
    }

    return lodash.map(features, (zippeds) => {
        json = {};

        lodash.each(zippeds, zip => {
            if (zip && zip[0] && zip[1]) {
                json[zip[0]] = zip[1];
            }
        });

        return json;
    });
}

module.exports = {
    qr_carousel,
    qr_button,
    qr_list,    
    quickreplies,
    generic,
    image,
    button
};
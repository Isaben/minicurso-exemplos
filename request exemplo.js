/*
    Realizar algumas requisições e trata as mesmas de acordo.
*/

const request = require("request");

/*
    Uma simples requisição a um site sem especificar o método ou qualquer
    outra opção. Por padrão, a biblioteca assume que estamos realizando
    uma requisição do tipo GET.
    O primeiro parâmetro é o link/JSON com as opções, e o segundo
    é um callback com uma função que deve obrigatoriamente conter
    3 argumentos:
    error: erro na requisição, caso existe. Assume valor null caso nada
    tenha dado errado.
    response: um objeto contendo TODAS as informações de sua requisição,
    e da resposta do servidor.
    body: o corpo da resposta
*/

request("http://motherfuckingwebsite.com/", (error, response, body) => {
    /*
        Como estamos realizando uma requisição de GET para uma página de uma site,
        é equivalente a visitarmos ele pelo navegador. O body assume como valor
        o HTML da págnia em questão
    */
    console.log(body);
})


/*
    Exemplo especificando as opções no primeiro parâmetro da chamada da função.
    Temos:
    uri: link que será direcionada a requisição
    method: método HTTP (GET, POST, etc) da requisição
    headers: Headers da requisição. Aqui é onde será colocado certas opções,
    como Content-Type para especificar o tipo do dado que está sendo enviado,
    Authorization para caso seja necessário, Content-Length, e entre outros.
    json: como temos um header de Content-Type: application/json, essa opção
    é essencial. Caso o tipo do conteúdo seja diferente, como um
    XML ou forms, ele não é necessário.
*/
request({
    uri: "https://httpbin.org/post",
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    json: {
        "teste": "oi"
    },
},  (error, response, body) => {
    console.log(body);
})

